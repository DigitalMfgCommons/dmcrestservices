#!/bin/bash
sudo yum update -y
sudo yum install -y java-1.8.0-openjdk.x86_64
sudo yum erase -y java-1.7.0-openjdk
sudo yum install -y git
#wget http://repos.fedorapeople.org/repos/dchen/apache-maven/epel-apache-maven.repo -O /etc/yum.repos.d/epel-apache-maven.repo
#sed -i s/\$releasever/6/g /etc/yum.repos.d/epel-apache-maven.repo
#yum install -y apache-maven
sudo yum install -y tomcat7
sudo service tomcat7 start
mkdir -p DMC
cd DMC
rm -rf *
git clone https://bitbucket.org/DigitalMfgCommons/dmcrestservices.git
cd dmcrestservices/target

# Use WAR with swagger on Development 
cp dmc-site-services-0.1.0-swagger.war rest.war
# Use WAR without swagger on productoin
#cp dmc-site-services-0.1.0.war rest.war

sudo cp rest.war /var/lib/tomcat7/webapps

######BEGIN EDIT#######
sudo echo "DBip=$DBip" >> /etc/tomcat7/tomcat7.conf
sudo echo "DBport=$DBport" >> /etc/tomcat7/tomcat7.conf
sudo echo "DBuser=$DBuser" >> /etc/tomcat7/tomcat7.conf
sudo echo "DBpass=$DBpass" >> /etc/tomcat7/tomcat7.conf
#####END EDIT##########

sudo service tomcat7 restart



