#!/bin/bash
rm -rf dmcrestservices
git clone https://bitbucket.org/DigitalMfgCommons/dmcrestservices.git
cd dmcrestservices/target
# Use WAR with swagger on Development 
sudo cp dmc-site-services-0.1.0-swagger.war /var/lib/tomcat7/webapps/rest.war
# Use WAR without swagger on productoin
# sudo cp dmc-site-services-0.1.0.war /var/lib/tomcat7/webapps/rest.war
sudo service tomcat7 restart

